angular.DZApp = angular.DZApp || {}
angular.DZApp.initSkillsApp = (element) ->
    $wrapper = angular.element element
    # Ignore when already bootstraped
    if $wrapper.hasClass 'ng-scope'
        return
        
    appName = $wrapper.data 'skills-app'
    appValue = $wrapper.data 'skills-value'
    
    angular
        .module appName, []
        .controller('SkillsController', ['$scope', ($scope) ->
            $scope.model =
                "q": []
                "w": []
                "e": []
                "r": []
                "stats": []
                
            $scope.value = ->
                JSON.stringify($scope.model)
                
            $scope.toggle = (skill, level) ->
                skill_model = null
                level_index = null

                # Iterate through the skill model
                angular.forEach $scope.model, (skill_model, key) ->
                    level_index = skill_model.indexOf level
                    
                    # If level already enabled
                    if level_index != -1
                        # Remove level from skill model
                        skill_model.splice level_index, 1
                    else
                        # Else enable level for the specified skill model
                        if key == skill
                            skill_model.push(level);
            
            $scope.isActive = (skill, level) ->
                $scope.model[skill].indexOf(level) != -1
            
            # Init skills value
            try
                angular.forEach $scope.model, (skill_model, skill_key) ->
                    if angular.isArray(appValue[skill_key])
                        $scope.model[skill_key] = appValue[skill_key]
            catch e
                # Ignore
                console.log(e.message);
        ])
    angular.bootstrap $wrapper, [appName];

angular.element(document).ready ->
    angular.element('[data-skills-app]').each ->
        angular.DZApp.initSkillsApp(this)
