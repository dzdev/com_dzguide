(function() {
  angular.DZApp = angular.DZApp || {};

  angular.DZApp.initSkillsApp = function(element) {
    var $wrapper, appName, appValue;
    $wrapper = angular.element(element);
    if ($wrapper.hasClass('ng-scope')) {
      return;
    }
    appName = $wrapper.data('skills-app');
    appValue = $wrapper.data('skills-value');
    angular.module(appName, []).controller('SkillsController', [
      '$scope', function($scope) {
        var e;
        $scope.model = {
          "q": [],
          "w": [],
          "e": [],
          "r": [],
          "stats": []
        };
        $scope.value = function() {
          return JSON.stringify($scope.model);
        };
        $scope.toggle = function(skill, level) {
          var level_index, skill_model;
          skill_model = null;
          level_index = null;
          return angular.forEach($scope.model, function(skill_model, key) {
            level_index = skill_model.indexOf(level);
            if (level_index !== -1) {
              return skill_model.splice(level_index, 1);
            } else {
              if (key === skill) {
                return skill_model.push(level);
              }
            }
          });
        };
        $scope.isActive = function(skill, level) {
          return $scope.model[skill].indexOf(level) !== -1;
        };
        try {
          return angular.forEach($scope.model, function(skill_model, skill_key) {
            if (angular.isArray(appValue[skill_key])) {
              return $scope.model[skill_key] = appValue[skill_key];
            }
          });
        } catch (_error) {
          e = _error;
          return console.log(e.message);
        }
      }
    ]);
    return angular.bootstrap($wrapper, [appName]);
  };

  angular.element(document).ready(function() {
    return angular.element('[data-skills-app]').each(function() {
      return angular.DZApp.initSkillsApp(this);
    });
  });

}).call(this);
