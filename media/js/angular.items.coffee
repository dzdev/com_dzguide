angular.DZApp = angular.DZApp || {}
angular.DZApp.initItemsApp = (element) ->
    $wrapper = angular.element element
    # Ignore when already bootstraped
    if $wrapper.hasClass 'ng-scope'
        return
        
    appName = $wrapper.data 'items-app'
    appValue = $wrapper.data 'items-value'
    appRoot = $wrapper.data 'items-app-root'
    $appPopup = $wrapper.find '.items-popup'
    
    angular
        .module appName, []
        .controller('ItemsController', ['$scope', '$http', ($scope, $http) ->
            orderPlaceholder =
                name: null
                items: []
            
            $scope.items = angular.DZApp.items;
            $scope.orders = [angular.copy(orderPlaceholder)];
            $scope.currentOrder = null;
            
            # Serialize model for database storage
            $scope.value = ->
                filtered = angular.copy($scope.orders)
                attribs = ['id', 'name', 'dname', 'attrib', 'cd', 'components', 'cost',
                    'created', 'desc', 'lore', 'mc', 'qual', 'params', 'image', 'link']
                removeQueue = []
                
                angular.forEach filtered, (order, index) ->
                    # Mark empty orders
                    if (!order.name || order.name.length == 0) && order.items.length == 0
                        removeQueue.push index
                    
                    angular.forEach order.items, (item) ->
                        angular.forEach item, (value, key) ->
                            # Remove unnecessary attributes from items
                            if attribs.indexOf(key) == -1
                                delete item[key];
                            # Clean domain from images link
                            if key == 'image'
                                item[key]['dota2']['lg'] = item[key]['dota2']['lg'].replace(/^.*\/\/[^\/]+/, '')
                                item[key]['dota1']['lg'] = item[key]['dota1']['lg'].replace(/^.*\/\/[^\/]+/, '')
                            
                # Remove empty orders
                angular.forEach removeQueue, (order_index, index) ->
                    filtered.splice order_index - index, 1
                
                # Return json encoded orders
                if filtered.length > 0
                    JSON.stringify(filtered);
                else
                    null
            
            # Add new order to order list
            $scope.addOrder = ->
                $scope.orders.push angular.copy(orderPlaceholder)
            
            # Remove order from order list
            $scope.removeOrder = (index) ->
                $scope.orders.splice index, 1
                
            # Start an order session with specified order
            $scope.startOrder = (order) ->
                $scope.currentOrder = order;
            
            # Add item to current session's order
            $scope.addItem = (item) ->
                if $scope.currentOrder
                    $scope.currentOrder.items.push item
                    
            # Remove item from specified order
            $scope.removeItem = (order, index) ->
                order.items.splice index, 1
                    
            # End order session
            $scope.endOrder = ->
                $scope.currentOrder = null
            
            # Init value
            if angular.isArray(appValue)
                $scope.orders = appValue
            else
                $scope.orders = [angular.copy(orderPlaceholder)]
            
        ])
    angular.bootstrap($wrapper, [appName]);
    
angular.element(document).ready ->
    angular.element('[data-items-app]').each ->
        angular.DZApp.initItemsApp(this)
