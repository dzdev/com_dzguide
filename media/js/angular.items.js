(function() {
  angular.DZApp = angular.DZApp || {};

  angular.DZApp.initItemsApp = function(element) {
    var $appPopup, $wrapper, appName, appRoot, appValue;
    $wrapper = angular.element(element);
    if ($wrapper.hasClass('ng-scope')) {
      return;
    }
    appName = $wrapper.data('items-app');
    appValue = $wrapper.data('items-value');
    appRoot = $wrapper.data('items-app-root');
    $appPopup = $wrapper.find('.items-popup');
    angular.module(appName, []).controller('ItemsController', [
      '$scope', '$http', function($scope, $http) {
        var orderPlaceholder;
        orderPlaceholder = {
          name: null,
          items: []
        };
        $scope.items = angular.DZApp.items;
        $scope.orders = [angular.copy(orderPlaceholder)];
        $scope.currentOrder = null;
        $scope.value = function() {
          var attribs, filtered, removeQueue;
          filtered = angular.copy($scope.orders);
          attribs = ['id', 'name', 'dname', 'attrib', 'cd', 'components', 'cost', 'created', 'desc', 'lore', 'mc', 'qual', 'params', 'image', 'link'];
          removeQueue = [];
          angular.forEach(filtered, function(order, index) {
            if ((!order.name || order.name.length === 0) && order.items.length === 0) {
              removeQueue.push(index);
            }
            return angular.forEach(order.items, function(item) {
              return angular.forEach(item, function(value, key) {
                if (attribs.indexOf(key) === -1) {
                  delete item[key];
                }
                if (key === 'image') {
                  item[key]['dota2']['lg'] = item[key]['dota2']['lg'].replace(/^.*\/\/[^\/]+/, '');
                  return item[key]['dota1']['lg'] = item[key]['dota1']['lg'].replace(/^.*\/\/[^\/]+/, '');
                }
              });
            });
          });
          angular.forEach(removeQueue, function(order_index, index) {
            return filtered.splice(order_index - index, 1);
          });
          if (filtered.length > 0) {
            return JSON.stringify(filtered);
          } else {
            return null;
          }
        };
        $scope.addOrder = function() {
          return $scope.orders.push(angular.copy(orderPlaceholder));
        };
        $scope.removeOrder = function(index) {
          return $scope.orders.splice(index, 1);
        };
        $scope.startOrder = function(order) {
          return $scope.currentOrder = order;
        };
        $scope.addItem = function(item) {
          if ($scope.currentOrder) {
            return $scope.currentOrder.items.push(item);
          }
        };
        $scope.removeItem = function(order, index) {
          return order.items.splice(index, 1);
        };
        $scope.endOrder = function() {
          return $scope.currentOrder = null;
        };
        if (angular.isArray(appValue)) {
          return $scope.orders = appValue;
        } else {
          return $scope.orders = [angular.copy(orderPlaceholder)];
        }
      }
    ]);
    return angular.bootstrap($wrapper, [appName]);
  };

  angular.element(document).ready(function() {
    return angular.element('[data-items-app]').each(function() {
      return angular.DZApp.initItemsApp(this);
    });
  });

}).call(this);
