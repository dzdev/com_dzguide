(function() {
  jQuery(document).ready(function() {
    jQuery('.btn-vote').tooltip({
      'placement': 'bottom',
      'trigger': 'hover'
    });
    return jQuery('[data-vote]').on('click', function(e) {
      var button, direction, id, type;
      e.preventDefault();
      button = this;
      if (jQuery(button).data('vote')) {
        id = jQuery(button).data('vote');
        type = jQuery(button).data('type');
        direction = jQuery(button).data('direction');
        jQuery(button).tooltip('destroy');
        jQuery(button).siblings('.btn-vote').tooltip('destroy');
        jQuery.ajax({
          url: '/index.php',
          data: {
            option: 'com_dzguide',
            task: type + '.' + direction + 'vote',
            id: id
          },
          dataType: 'json',
          success: function(data) {
            var $otherVoteBtn, $otherVoteCount, $thisVoteBtn, $thisVoteCount, other_direction;
            jQuery(button).tooltip({
              'placement': 'bottom',
              'title': data.message
            }).tooltip('show');
            setTimeout(function() {
              return jQuery(button).tooltip('hide');
            }, 2000);
            if (data.status === 'ok') {
              if (direction === 'up') {
                other_direction = 'down';
              } else {
                other_direction = 'up';
              }
              $thisVoteBtn = jQuery("[data-vote='" + id + "'][data-direction='" + direction + "']");
              $otherVoteBtn = jQuery("[data-vote='" + id + "'][data-direction='" + other_direction + "']");
              $thisVoteCount = jQuery("[data-vote-count-of='" + id + "'][data-vote-direction='" + direction + "']");
              $otherVoteCount = jQuery("[data-vote-count-of='" + id + "'][data-vote-direction='" + other_direction + "']");
              $thisVoteBtn.addClass('active');
              $thisVoteCount.text(parseInt($thisVoteCount.text()) + 1);
              if ($otherVoteBtn.hasClass('active')) {
                $otherVoteBtn.removeClass('active');
                return $otherVoteCount.text(parseInt($otherVoteCount.text()) - 1);
              }
            }
          }
        });
      }
      return false;
    });
  });

}).call(this);
