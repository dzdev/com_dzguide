<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Guide controller class.
 */
class DzguideControllerGuide extends JControllerForm
{

    function __construct() {
        $this->view_list = 'guides';
        parent::__construct();
    }
    
    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        $recordId = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $userId = $user->get('id');

        // Check general edit permission first.
        if ($user->authorise('core.edit', 'com_dzguide.guide.' . $recordId))
        {
            return true;
        }

        // Fallback on edit.own.
        // First test if the permission is available.
        if ($user->authorise('core.edit.own', 'com_dzguide.guide.' . $recordId))
        {
            // Now test the owner is the user.
            $ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId)
            {
                // Need to do a lookup from the model.
                $record = $this->getModel()->getItem($recordId);

                if (empty($record))
                {
                    return false;
                }

                $ownerId = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($ownerId == $userId)
            {
                return true;
            }
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     *
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     *
     * @return    void
     *
     * @since    3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {
        if (is_array($validData['builds']) && count($validData['builds'])) {
            $guide_id = (int) $model->getItem()->id;
            $db = JFactory::getDbo();
            
            // Clear all associated builds of this guide
            $query = $db->getQuery(true);
            $query->update('#__dzguide_builds')
                ->set('guide_id = 0')
                ->set('state = 0')
                ->where('guide_id = ' . $guide_id);
            $db->setQuery($query);
            $db->execute();
            
            // Associate new builds to this guide
            foreach($validData['builds'] as $build) {
                // Set build to this guide
                $build['guide_id'] = $guide_id;
                $build['hero_id'] = $validData['hero_id'];
                $build['state'] = $validData['state'];
                
                // Get build model
                $model = JModelLegacy::getInstance('Build', 'DZGuideModel');
                $form = $model->getForm($build, false);
                
                if ($form) {
                    $validBuildData = $model->validate($form, $build);
                    
                    if ($validBuildData) {
                        $model->save($validBuildData);
                    }
                }
            }
        }
        return;
    }
}
