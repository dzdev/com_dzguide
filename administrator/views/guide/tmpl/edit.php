<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JForm::addFormPath(JPATH_COMPONENT . '/models/forms/');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dzguide/assets/css/dzguide.css');
$add_build_title = JText::_('COM_DZGUIDE_TAB_TITLE_ADD_BUILD');
$new_build_title = JText::_('COM_DZGUIDE_TAB_TITLE_NEW_BUILD');
$document->addScriptDeclaration(<<<ADDTAB
jQuery(window).load(function() {
    var tab = jQuery('<li class=""><a id="add_build" href="#"><i class="icon-plus"></i>{$add_build_title}</a></li>');
    var nextBuildIndex = function() {
        var i = 0;
        while (jQuery("#build_" + i).length > 0) {
            i++;
        }
        return i;
    }
    removeBuild = function(button) {
        if (confirm("Are you sure?")) {
            var \$tab = jQuery(button).closest('.tab-pane');
            jQuery('li a[href="#' + \$tab.attr('id') + '"]').parent().remove();
            \$tab.remove();
        }
        return false;
    }
    jQuery('#buildTabTabs').append(tab);
    jQuery('#add_build').on('click', function() {
        jQuery('#add_build .icon-plus').addClass('rotating');
        jQuery.get('index.php?option=com_dzguide&view=guide&layout=edit', function(html) {
            jQuery('#add_build .icon-plus').removeClass('rotating');
            var \$document = jQuery(html),
                index = nextBuildIndex(),
                \$tab_nav = jQuery('<li class="active"><a href="#build_' + index + '">{$new_build_title}</a></li>'),
                \$tab_content = jQuery(\$document.find('#build_0')[0].outerHTML.replace(/_0/g, '_' + index).replace(/\[0\]/g, '[' + index + ']'));
            
            // Deactivate current tab
            jQuery('#builds li.active, #builds .tab-pane.active').removeClass('active');
            
            // Add new tab to builds
            \$tab_nav.insertBefore('#buildTabTabs li:last-child');
            \$tab_nav.find('a').on('click', function(e) {
                e.preventDefault();
                jQuery(this).tab('show');
            });
            angular.DZApp.initItemsApp(\$tab_content.find("[data-items-app]"));
            angular.DZApp.initSkillsApp(\$tab_content.find("[data-skills-app]"));
            \$tab_content.appendTo('#builds .tab-content');
        })
    });
});
ADDTAB
);
?>
<script type="text/javascript">
    jQuery.noConflict();
    Joomla.submitbutton = function(task)
    {
        if (task == 'guide.cancel') {
            Joomla.submitform(task, document.getElementById('guide-form'));
        }
        else {
            
            if (task != 'guide.cancel' && document.formvalidator.isValid(document.id('guide-form'))) {
                
                Joomla.submitform(task, document.getElementById('guide-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
    var reloadBuild = function() {
        jQuery("#builds-wrapper .icon-loop").addClass("rotating");
        jQuery("#builds-wrapper").load(document.URL + ' #builds-wrapper > *', function() {
            jQuery("#builds-wrapper select").chosen();
        });
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dzguide&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="guide-form" class="form-validate">
    <?php echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DZGUIDE_TITLE_GUIDE', true)); ?>
        <div class="row-fluid">
            <div class="span9">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('hero_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('hero_id'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('content'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('content'); ?></div>
                </div>
            </div>
            <div class="span3">
                <!-- Begin Sidebar -->
                <?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
                <!-- End Sidebar -->
                <fieldset class="form-vertical">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('cover'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('cover'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('patch_version'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('patch_version'); ?></div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'builds', JText::_('COM_DZGUIDE_BUILDS', true)); ?>
            <?php echo JHtml::_('bootstrap.startTabSet', 'buildTab', array('active' => 'build_0')); ?>
            <?php if (isset($this->item->builds_model) && count($this->item->builds_model)) : ?>
                <?php foreach($this->item->builds_model as $index => $build) : ?>
                    <?php echo JHtml::_('bootstrap.addTab', 'buildTab', 'build_' . $index, JText::sprintf('COM_DZGUIDE_TAB_TITLE_BUILD', $index + 1)); ?>
                        <?php $this->item->build_form = JForm::getInstance('com_dzguide.build.' . $index, 'build', array('control' => "jform[builds][$index]")); ?>
                        <?php $this->item->build_form->bind($build); ?>
                        <?php echo $this->loadTemplate('build'); ?>
                    <?php echo JHtml::_('bootstrap.endTab'); ?>
                <?php endforeach; ?>
            <?php else : ?>
                <?php echo JHtml::_('bootstrap.addTab', 'buildTab', 'build_0', JText::_('COM_DZGUIDE_TAB_TITLE_NEW_BUILD')); ?>
                    <?php $this->item->build_form = JForm::getInstance('com_dzguide.build.0', 'build', array('control' => "jform[builds][0]")); ?>
                    <?php echo $this->loadTemplate('build'); ?>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>
            <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DZGUIDE_PUBLISHING', true)); ?>
        <div class="row-fluid">
            <div class="span6">
                 <?php echo JLayoutHelper::render('joomla.edit.publishingdata', $this); ?>
            </div>
            <div class="span6">
                <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php if (JFactory::getUser()->authorise('core.admin','dzguide')) : ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
                <?php echo $this->form->getInput('rules'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
