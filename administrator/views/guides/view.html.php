<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Dzguide.
 */
class DzguideViewGuides extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        DzguideHelper::addSubmenu('guides');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/dzguide.php';

        $state = $this->get('State');
        $canDo = DzguideHelper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_DZGUIDE_TITLE_GUIDES'), 'guides.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/guide';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolBarHelper::addNew('guide.add', 'JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('guide.edit', 'JTOOLBAR_EDIT');
            }
        }

        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('guides.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('guides.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'guides.delete', 'JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('guides.archive', 'JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('guides.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }

        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'guides.delete', 'JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('guides.trash', 'JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_dzguide');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_dzguide&view=guides');

        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);

		//Filter for the field version
		$select_label = JText::sprintf('COM_DZGUIDE_FILTER_SELECT_LABEL', 'Version');
		$options = array();
        $params = JComponentHelper::getParams('com_dzguide');
        $versions = explode(',', $params->get('versions', '6.83,6.84,6.84b', 'string'));
        foreach ($versions as $version) {
            $options[] = JHtml::_('select.option', $version, $version);
        }
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_patch_version',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.patch_version'), true)
		);
        
        // Filter by hero
        $select_label = JText::sprintf('COM_DZGUIDE_FILTER_SELECT_LABEL', 'Hero');
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)->select('id as value, dname as text FROM #__dota2_heroes');
        $db->setQuery($query);
        $options = $db->loadObjectList();
        $option_no_hero = new stdClass();
        $option_no_hero->value = "-1";
        $option_no_hero->text = JText::_('COM_DZGUIDE_FORM_OPTION_NO_HERO');
        array_unshift($options, $option_no_hero);
        
        JHtmlSidebar::addFilter(
			$select_label,
			'filter_hero',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.hero'), true)
		);
    }

	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		'a.created' => JText::_('COM_DZGUIDE_GUIDES_CREATED'),
		'a.created_by' => JText::_('COM_DZGUIDE_GUIDES_CREATED_BY'),
		'a.title' => JText::_('COM_DZGUIDE_GUIDES_TITLE'),
		'a.hits' => JText::_('COM_DZGUIDE_GUIDES_HITS'),
		'a.version' => JText::_('COM_DZGUIDE_GUIDES_VERSION'),
		);
	}

}
