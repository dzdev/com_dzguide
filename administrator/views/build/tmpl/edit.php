<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dzguide/assets/css/dzguide.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    js('input:hidden.hero_id').each(function(){
        var name = js(this).attr('name');
        if(name.indexOf('hero_idhidden')){
            js('#jform_hero_id option[value="'+js(this).val()+'"]').attr('selected',true);
        }
    });
    js("#jform_hero_id").trigger("liszt:updated");
    js('input:hidden.guide_id').each(function(){
        var name = js(this).attr('name');
        if(name.indexOf('guide_idhidden')){
            js('#jform_guide_id option[value="'+js(this).val()+'"]').attr('selected',true);
        }
    });
    js("#jform_guide_id").trigger("liszt:updated");
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'build.cancel') {
            Joomla.submitform(task, document.getElementById('build-form'));
        }
        else {
            
            if (task != 'build.cancel' && document.formvalidator.isValid(document.id('build-form'))) {
                
                Joomla.submitform(task, document.getElementById('build-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dzguide&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="build-form" class="form-validate">
    <?php echo JLayoutHelper::render('joomla.edit.title_alias', $this); ?>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DZGUIDE_TITLE_BUILD', true)); ?>
        <div class="row-fluid">
            <div class="span9">
                <fieldset class="adminform">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hero_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hero_id'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('guide_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('guide_id'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                    </div>
                </fieldset>
            </div>
            <div class="span3">
                <!-- Begin Sidebar -->
                <?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
                <!-- End Sidebar -->
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'skills_items', JText::_('COM_DZGUIDE_TITLE_SKILLS_ITEMS', true)); ?>
        <div class="row-fluid">
            <div class="span12">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('skills'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('skills'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('items'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('items'); ?></div>
                </div>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DZGUIDE_PUBLISHING', true)); ?>
        <div class="row-fluid">
            <div class="span6">
                 <?php echo JLayoutHelper::render('joomla.edit.publishingdata', $this); ?>
            </div>
            <div class="span6">
                <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php if (JFactory::getUser()->authorise('core.admin','dzguide')) : ?>
    <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
        <?php echo $this->form->getInput('rules'); ?>
    <?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
