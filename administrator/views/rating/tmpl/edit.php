<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dzguide/assets/css/dzguide.css');
?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var $guide_select = jQuery("#jform_guide_id"),
            $build_select = jQuery("#jform_build_id");
        $guide_select.on('change', function() {
            if ($guide_select.val().length > 0) {
                $build_select.attr('disabled', 'disabled');
            } else {
                $build_select.attr('disabled', null);
            }
            $build_select.trigger("liszt:updated.chosen");
        });
        $build_select.on('change', function() {
            if ($build_select.val().length > 0) {
                $guide_select.attr('disabled', 'disabled');
            } else {
                $guide_select.attr('disabled', null);
            }
            $guide_select.trigger("liszt:updated.chosen");
        });
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'rating.cancel') {
            Joomla.submitform(task, document.getElementById('rating-form'));
        }
        else {
            
            if (task != 'rating.cancel' && document.formvalidator.isValid(document.id('rating-form'))) {
                
                Joomla.submitform(task, document.getElementById('rating-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dzguide&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="rating-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DZGUIDE_TITLE_RATING', true)); ?>
        <div class="row-fluid">
            <div class="span9 form-horizontal">
                <fieldset class="adminform">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('state'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('guide_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('guide_id'); ?></div>
                    </div>

                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('build_id'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('build_id'); ?></div>
                    </div>
                   <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('useful'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('useful'); ?></div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DZGUIDE_PUBLISHING', true)); ?>
        <div class="row-fluid">
            <div class="span6">
                 <?php echo JLayoutHelper::render('joomla.edit.publishingdata', $this); ?>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php if (JFactory::getUser()->authorise('core.admin','dzguide')) : ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
                <?php echo $this->form->getInput('rules'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
