<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_dota2/models');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldItems extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'items';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		JHtml::_('jquery.framework');
		
		$items = json_encode($this->getItems());
		JFactory::getDocument()->addScriptDeclaration(<<<SCRIPT
		angular.DZApp = angular.DZApp || {};
		angular.DZApp.items = $items;
SCRIPT
);
		JHtml::_('script','com_dzguide/angular.min.js', false, true);
		JHtml::_('script', 'com_dzguide/angular.items.js', false, true);
		JHtml::_('stylesheet', 'com_dzguide/angular.items.css', false, true);
		$root = JUri::root();
		$this->value = htmlspecialchars($this->value);
		if (!function_exists('translate')) {
			function translate($string) {
				return JText::_($string);
			}
		}
		$t = "translate";
		
		$html = <<<INPUT
		<div class="items-app" data-items-app='{$this->id}_app' data-items-value="{$this->value}" ng-controller="ItemsController" ng-click="endOrder()">
			<div class="items-orders">
				<div class="items-order" ng-repeat="order in orders">
					<label style="display: none;" for="order_{{\$index}}_name">{$t('COM_DZGUIDE_FIELD_ITEMS_ORDER_NAME')}</label>
					<input type="text" id="order_{{\$index}}_name" ng-model="order.name" ng-required="order.items.length > 0" placeholder="{$t('COM_DZGUIDE_FIELD_ITEMS_ORDER_NAME')}..." class="form-control order-name" />
					<img class="item" ng-repeat="item in order.items track by \$index"
						ng-src="{{ item.image.dota2.lg }}" title="{{ item.dname }}"
						ng-click="removeItem(order, \$index)" />
					<span class="item placeholder" ng-click="startOrder(order); \$event.stopPropagation();">+</span>
					
					<div class="items-popup active" ng-show="currentOrder == order" ng-click="\$event.stopPropagation();">
						<div class="arrow-up"></div>
						<a class="btn btn-link btn-close" ng-click="endOrder()">x</a>
						<div class="items-filter">
							<div class="input-prepend">
								<span class="add-on"><i class="icon-filter"></i></span>
								<input type="text" ng-model="search.dname" />
							</div>
						</div>
						<div class="items-list">
							<div class="item" ng-repeat="item in items | filter:search" title="{{ item.dname }}" ng-click="addItem(item)">
								<img ng-src="{{ item.image.dota2.lg }}" />
								<span>{{ item.dname }}</span>
							</div>
						</div>
					</div>
					<a class="btn-close btn-link" ng-click="removeOrder(\$index)">x</a>
				</div>
				<div class="items-row-add btn btn-default" ng-click="addOrder()">{$t('COM_DZGUIDE_FIELD_ITEMS_BTN_ADD_ORDER')}</div>
			</div>
			<input id='{$this->id}' type='hidden' name='{$this->name}' ng-value='value()' />
		</div>
INPUT;

		return $html;
	}
	
	private function getItems()
	{
		$model = JModelLegacy::getInstance('Items', 'Dota2Model', array('ignore_request' => true));
		$model->setState('list.limit', 999);
		
		return $model->getItems();
	}
}
