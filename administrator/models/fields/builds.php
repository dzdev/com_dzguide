<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldBuilds extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'builds';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$app = JFactory::getApplication();
		$id = $app->input->get('id', 0, 'int');
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select('id, title')
			->from('#__dzguide_builds');
		if ($id) {
			$query->where('guide_id = 0 OR guide_id = ' . $id);
		} else {
			$query->where('guide_id = 0');
		}
		
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		$input_options = 'class="' . $this->getAttribute('class') . '"';
		
		$options = array();

		//Iterate through all the results
		foreach ($results as $result)
		{
			$options[] = JHtml::_('select.option', $result->id, $result->title);
		}

		$value = $this->value;

		//If the value is a string -> Only one result
		if (is_string($value))
		{
			$value = array($value);
		}
		else if (is_object($value))
		{ //If the value is an object, let's get its properties.
			$value = get_object_vars($value);
		}

		//If the select is multiple
		if ($this->multiple)
		{
			$input_options .= 'multiple="multiple"';
		}
		else
		{
			array_unshift($options, JHtml::_('select.option', '', ''));
		}

		$html = JHtml::_('select.genericlist', $options, $this->name, $input_options, 'value', 'text', $value, $this->id);

		return $html;
	}
	
	/**
	 * Wrapper method for getting attributes from the form element
	 *
	 * @param string $attr_name Attribute name
	 * @param mixed  $default   Optional value to return if attribute not found
	 *
	 * @return mixed The value of the attribute if it exists, null otherwise
	 */
	public function getAttribute($attr_name, $default = null)
	{
		if (!empty($this->element[$attr_name]))
		{
			return $this->element[$attr_name];
		}
		else
		{
			return $default;
		}
	}
}
