<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldSkills extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'skills';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		JHtml::_('script','com_dzguide/angular.min.js', false, true);
		JHtml::_('script', 'com_dzguide/angular.skills.js', false, true);
		JHtml::_('stylesheet', 'com_dzguide/angular.skills.css', false, true);
		
		$html = <<<INPUT
			<div data-skills-app='{$this->id}_app' data-skills-value='{$this->value}' ng-controller='SkillsController'>
				<table class='table table-bordered table-skills'>
					<tbody>
						<tr ng-repeat="skill in ['q','w','e','r','stats']">
							<th>{{ skill.toUpperCase() }}</th>
							<td
								ng-repeat="i in [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]"
								ng-class='{"active": isActive(skill, i)}'
								ng-click='toggle(skill,i)'>
								{{ i }}
							</td>
						</tr>
					</tbody>
				</table>
				<input id='{$this->id}' type='hidden' name='{$this->name}' ng-value='value()' />
			</div>
INPUT;
		return $html;
	}
}
