<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('JPATH_BASE') or die;

jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldVersions extends JFormFieldList
{
    /**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'versions';
    
    protected function getOptions()
    {
        $params = JComponentHelper::getParams('com_dzguide');
        $versions = explode(',', $params->get('versions', '6.83,6.84,6.84b'));
        
        $options = parent::getOptions();
        
        foreach ($versions as $version) {
            $options[] = JHtml::_('select.option', trim($version), trim($version));
        }
        
        return $options;
    }
}
