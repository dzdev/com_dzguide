<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Dzguide model.
 */
class DzguideModelGuide extends JModelAdmin
{
    /**
     * @var        string    The prefix to use with controller messages.
     * @since    1.6
     */
    protected $text_prefix = 'COM_DZGUIDE';


    /**
     * Returns a reference to the a Table object, always creating it.
     *
     * @param    type    The table type to instantiate
     * @param    string    A prefix for the table class name. Optional.
     * @param    array    Configuration array for model. Optional.
     * @return    JTable    A database object
     * @since    1.6
     */
    public function getTable($type = 'Guide', $prefix = 'DzguideTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the record form.
     *
     * @param    array    $data        An optional array of data for the form to interogate.
     * @param    boolean    $loadData    True if the form is to load its own data (default case), false if not.
     * @return    JForm    A JForm object on success, false on failure
     * @since    1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Initialise variables.
        $app    = JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm('com_dzguide.guide', 'guide', array('control' => 'jform', 'load_data' => $loadData));
        
        
        if (empty($form)) {
            return false;
        }
        
        $user = JFactory::getUser();

        // Check for existing article.
        // Modify the form based on Edit State access controls.
        $id = JFactory::getApplication()->input->get('id', 0);
        if ($id != 0 && (!$user->authorise('core.edit.state', 'com_dzguide.guide.' . (int) $id))
            || ($id == 0 && !$user->authorise('core.edit.state', 'com_dzguide')))
        {
            // Disable fields for display.
            $form->setFieldAttribute('state', 'disabled', 'true');

            // Disable fields while saving.
            // The controller has already verified this is an article you can edit.
            $form->setFieldAttribute('state', 'filter', 'unset');
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return    mixed    The data for the form.
     * @since    1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_dzguide.edit.guide.data', array());

        if (empty($data)) {
            $data = $this->getItem();
            
        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param    integer    The id of the primary key.
     *
     * @return    mixed    Object on success, false on failure.
     * @since    1.6
     */
    public function getItem($pk = null)
    {
        if ($item = parent::getItem($pk)) {
            //Do any procesing on fields here if needed
            
            // Load associated builds into this guide
            if ($item->id) {
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                
                $query->select('id, title, alias, items, skills, description')
                    ->from('#__dzguide_builds')
                    ->where('guide_id = ' . $item->id);
                $db->setQuery($query);
                $item->builds_model = $db->loadObjectList();
                
                $item->tags = new JHelperTags;
                $item->tags->getTagIds($item->id, 'com_dzguide.guide');
            }
        }

        return $item;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @since    1.6
     */
    protected function prepareTable($table)
    {
        jimport('joomla.filter.output');

        if (empty($table->id)) {

            // Set ordering to the last item if not set
            if (@$table->ordering === '') {
                $db = JFactory::getDbo();
                $db->setQuery('SELECT MAX(ordering) FROM #__dzguide_guides');
                $max = $db->loadResult();
                $table->ordering = $max+1;
            }

        }
    }

    /**
     * Override publish to sync builds' status with guide's one
     */
    public function publish(&$pks, $value = 1)
    {
        if (parent::publish($pks, $value))
        {
            // Get build IDs
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                ->from('#__dzguide_builds')
                ->where('guide_id IN (' . implode($pks, ',') . ')');
            $db->setQuery($query);
            $ids = $db->loadColumn();
            
            // Build model
            $model = JModelLegacy::getInstance('Build', 'DZGuideModel');
            $model->publish($ids, $value);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Override delete to remove associated builds
     */
    public function delete(&$pks)
    {
        if (parent::delete($pks))
        {
            // Get build IDs
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('id')
                ->from('#__dzguide_builds')
                ->where('guide_id IN (' . implode($pks, ',') . ')');
            $db->setQuery($query);
            $ids = $db->loadColumn();
            
            // Build model
            $model = JModelLegacy::getInstance('Build', 'DZGuideModel');
            $model->delete($ids);
            
            return true;
        }
        
        return false;
    }
}
