<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Dzguide helper.
 */
class DzguideHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
        JHtmlSidebar::addEntry(
			JText::_('COM_DZGUIDE_TITLE_GUIDES'),
			'index.php?option=com_dzguide&view=guides',
			$vName == 'guides'
		);
        
		JHtmlSidebar::addEntry(
			JText::_('COM_DZGUIDE_TITLE_BUILDS'),
			'index.php?option=com_dzguide&view=builds',
			$vName == 'builds'
		);
		
		JHtmlSidebar::addEntry(
			JText::_('COM_DZGUIDE_TITLE_RATINGS'),
			'index.php?option=com_dzguide&view=ratings',
			$vName == 'ratings'
		);

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_dzguide';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


}
