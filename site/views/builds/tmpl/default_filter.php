<?php

defined('JPATH_BASE') or die;

$view = $displayData['view'];
$form = $view->filterForm;
$state = $view->get('State');
$filters = array(
    'filter' => array(
        'search' => $state->get('filter.search'),
        'hero_id' => $state->get('filter.hero_id')
    ),
    'list' => array(
        'fullordering' => strtolower($state->get('list.ordering')) . ' ' . strtolower($state->get('list.direction'))
    )
);
$form->bind($filters);

JFactory::getDocument()->addScriptDeclaration(<<<SCRIPT
jQuery(document).ready(function() {
    jQuery('button[type="reset"]').on('click', function(e) {
        e.preventDefault();
        
        jQuery('input, select', this.form).val('');
        jQuery(this.form).submit();
        
        return false;
    });
});
SCRIPT
);
?>
<div class="filters row-fluid form-inline">
    <div class="form-group">
        <?= $form->getField('search', 'filter')->input; ?>
    </div>
    <div class="form-group">
        <?= $form->getField('hero_id', 'filter')->input; ?>
    </div>
    <div class="form-group">
        <?= $form->getField('fullordering', 'list')->input; ?>
    </div>
    <div class="form-group pull-right">
        <button type="submit" class="btn btn-primary">Search</button>
        <button type="reset" class="btn btn-default">Clear</button>
    </div>
</div>
