<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canCreate = $user->authorise('core.create', 'com_dzguide');
$canEdit = $user->authorise('core.edit', 'com_dzguide');
$canCheckin = $user->authorise('core.manage', 'com_dzguide');
$canChange = $user->authorise('core.edit.state', 'com_dzguide');
$canDelete = $user->authorise('core.delete', 'com_dzguide');
$root = JUri::root();
JFactory::getDocument()->addScriptDeclaration(<<<SCRIPT
jQuery(document).ready(function() {
    var hide_tables = function(container) {
        jQuery(".build-tables", container).hide('400');
    }
    jQuery(".build-container").on("click", function() {
        jQuery(".build-tables", this).toggle('400');
        jQuery(this).siblings(".build-container").each(function() {
            hide_tables(this);
        });
    });
    jQuery('#adminForm').on('submit', function() {
        jQuery('input, select', this).each(function() {
            if (jQuery(this).val() === '') {
                jQuery(this).attr('disabled', 'disabled');
            }
        });
        
        return true;
    });
    jQuery('[data-toggle="tooltip"]').tooltip();
});
SCRIPT
);
JHtml::_('jquery.framework');
JHtml::script('com_dzguide/vote.js', false, true);
?>

<form action="<?php echo JRoute::_('index.php?option=com_dzguide&view=builds'); ?>" method="GET" name="adminForm" id="adminForm">
    <?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
    <br /><br />
    <?php foreach ($this->items as $item) : ?>
    <div class="build-container">
        <div class="row header">
            <div class="col-md-2">
                <img
                    src="<?= JUri::root().'media/com_dota2/images/dota2/heroes/'.$item->hero_name.'_vert.jpg'; ?>"
                    class="img-responsive" title="<?= $item->hero_dname; ?>" />
            </div>
            <div class="col-md-3">
                <h3><?= $item->title; ?></h3>
                <span class="build-author"><?= $item->created_by_name; ?></span> | <span class="build-modified">Updated <?= $item->modified; ?></span>
            </div>
            <div class="col-md-5">
                <?= $item->description; ?>
            </div>
            <div class="col-md-2">
                <?php if ($userId) : ?>
                    <a href="#" class="btn-vote<?= in_array( (string) $item->id, $this->userVotes['upvotes']) ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="build" data-direction="up"><i class="fa fa-thumbs-up"></i></a>
                <?php else : ?>
                    <a href="#" class="btn-vote disabled" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                        <i class="fa fa-thumbs-up"></i>
                    </a>
                <?php endif; ?>
                <span class="vote-count vote-up"><?= $item->upvotes; ?></span> / <span class="vote-count vote-down"><?= $item->downvotes; ?></span>
                <?php if ($userId) : ?>
                    <a href="#" class="btn-vote<?= in_array( (string) $item->id, $this->userVotes['downvotes']) ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="build" data-direction="down"><i class="fa fa-thumbs-down"></i></a>
                <?php else : ?>
                    <a href="#" class="btn-vote disabled" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="row build-tables" style="display: none">
            <div class="col-md-12">
                <h3>Skill Order</h3>
                <?php if ($item->skills) : ?>
                <table class="table table-bordered skill-table">
                    <?php foreach(array('q', 'w', 'e', 'r', 'stats') as $key) : ?>
                    <tr>
                        <th><?= strtoupper($key) ?></th>
                        <?php for ($i = 0; $i < 25; $i++) : ?>
                        <td <?= in_array($i, $item->skills->{$key}) ? 'style="background-color: #444" class="active"' : null; ?>>
                            <?= $i + 1; ?>
                        </td>
                        <?php endfor; ?>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            </div>
            <div class="col-md-12">
                <h3>Item Order</h3>
                <?php foreach ($item->items as $order) : ?>
                    <div class="order-container">
                        <h4><?= $order->name; ?></h4>
                        <ul>
                        <?php foreach ($order->items as $order_item) : ?>
                            <li><img src="<?= $order_item->image->dota2->lg; ?>" width="32px" height="32px" title="<?= $order_item->dname; ?>"/></li>
                        <?php endforeach; ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                <?php endforeach; ?>
            </div>
            
            <?php if (isset($item->guide_link)) : ?>
            <div class="col-md-12">
                <a href="<?= $item->guide_link; ?>" class="btn btn-primary pull-right">Open Guide</a>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
    
    <div class="pagination">
        <?php echo $this->pagination->getPagesLinks(); ?>
    </div>
</form>
<style type="text/css">
.build-container {
    cursor: pointer;
    padding: 20px 10px;
    border-bottom: 1px solid #222;
}
.build-container:hover {
    background-color: #222;
}
.skill-table > tbody > tr > td {
    padding: 8px 4px;
    text-align: center;
    min-width: 26px;
}
.order-container {
    background-color: #333;
    padding: 5px 10px;
    display: inline-block;
}
.order-container h4 {
    border-bottom: 1px solid #aaa;
}
.order-container ul {
    list-style: none;
    margin: 0;
    padding: 0;
}
.order-container ul li {
    float: left;
    list-style: none;
    padding: 1px 5px;
}
.btn-vote.disabled:hover {
    color: inherit;
}
.btn-vote.active {
    color: red;
}
</style>
