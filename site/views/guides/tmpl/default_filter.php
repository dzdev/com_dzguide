<?php

defined('JPATH_BASE') or die;
$view = $displayData['view'];
$form = $view->filterForm;
$state = $view->get('State');
$heroes = $view->get('Heroes');
$listOrder = $state->get('list.ordering');
$listDirn = $state->get('list.direction');
$fullOrder = "$listOrder $listDirn";
$filters = array(
    'q' => $state->get('filter.search'),
    'v' => $state->get('filter.patch_version'),
    'order' => $fullOrder
);
$form->bind($filters);
$menu = JFactory::getApplication()->getMenu()->getActive();
if ($menu) {
  $current_link = 'index.php?Itemid=' . $menu->id;
} else {
  $current_link = 'index.php?option=com_dzguide&view=guides';
}
$current_link .= '&' . http_build_query($filters);
?>
<div class="dropdown clearfix">
    <button class="btn btn-default dropdown-toggle" type="button" id="heroMenu" data-toggle="dropdown" aria-expanded="false">
    <?php foreach ($heroes as $hero) { ?>
      <?php if ($hero->id == $state->get('filter.hero_id')) { ?>
        <img src="<?= $hero->image; ?>" width="24" height="24" /> <?= $hero->dname; ?>
      <?php } ?>
    <?php } ?>
    <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="heroMenu">
      <?php foreach ($heroes as $hero) : ?>
      <li role="presentation" <?= ($hero->id == $state->get('filter.hero_id')) ? 'class="disabled"' : null; ?>>
        <a role="menuitem" tabindex="-1" href="<?= JRoute::_($current_link . '&hero_name=' . $hero->name) ?>">
          <img src="<?= $hero->image; ?>" width="24" height="24" /> <?= $hero->dname; ?>
        </a>
      </li>
      <?php endforeach; ?>
    </ul>
</div>
<br />
<div class="row">
    <div class="col-md-3"><div class="form-group"><?= $form->getField('q')->input; ?></div></div>
    <div class="col-md-3"><div class="form-group"><?= $form->getField('v')->input; ?></div></div>
    <div class="col-md-3">
        <div class="form-group">
            <select name="order" class="form-control">
                <option value="">- Sắp xếp -</option>
                <?php
                $sorts = array(
                    'a.created desc' => 'Guide mới nhất',
                    'a.hits desc' => 'Guide xem nhiều nhất',
                    'a.modified desc' => 'Guide mới cập nhật',
                );
                ?>
                <?php foreach ($sorts as $key => $value) : ?>
                <option value="<?= $key; ?>" <?= ($key == $fullOrder) ? 'selected' : '' ?>><?= $value; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-2 col-xs-10"><button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Tìm kiếm</button> </div>
    <div class="col-md-1 col-xs-2"><button type="reset" class="btn btn-primary btn-block"><i class="fa fa-close"></i></button></div>
</div>
<?php
$remove_endpoint = JRoute::_('index.php?option=com_dzguide&task=guideform.remove&id=', false, 2);
JFactory::getDocument()->addScriptDeclaration(<<<SCRIPT
    jQuery(document).ready(function () {
        var \$form= jQuery('form'),
        \$filter_order = jQuery('[name="filter_order"]'),
        \$filter_order_Dir = jQuery('[name="filter_order_Dir"]');
        jQuery('.delete-button').click(deleteItem);

        jQuery('button[type="reset"]').on('click', function(e) {
            e.preventDefault();

            \$form.find('input, select').val("");
            \$form.submit();

            return false;
        });

        // Disable empty input on submit
        \$form.on('submit', function() {
            \$form.find('input, select').each(function() {
                if (jQuery(this).val() === "") {
                    jQuery(this).attr('disabled', 'disabled');
                }
            });

            return true;
        });
    });

    function deleteItem() {
        var item_id = jQuery(this).attr('data-item-id');
        if (confirm("<?php echo JText::_('COM_DZGUIDE_DELETE_MESSAGE'); ?>")) {
            window.location.href = '$remove_endpoint' + item_id;
        }
    }
SCRIPT
);
?>
