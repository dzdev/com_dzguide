<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
JHtml::script('com_dzguide/vote.js', false, true);

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canCreate = $user->authorise('core.create', 'com_dzguide');
$canEdit = $user->authorise('core.edit', 'com_dzguide');
$canCheckin = $user->authorise('core.manage', 'com_dzguide');
$canChange = $user->authorise('core.edit.state', 'com_dzguide');
$canDelete = $user->authorise('core.delete', 'com_dzguide');
?>
<form action="" method="GET" name="adminForm" id="adminForm">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=219148504876145";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="component component-dark guide-blog">
	<div class="component-inner">
    	<div class="component-header">
    		<h1><?php echo $this->params->get('page_heading');?></h1>
    	</div>
        <div class="component-content">
        	<div class="guide-filters">
				<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
            </div>
            <table class="guide-list" width="100%">
			<?php foreach ($this->items as $i => $item) : ?>
            	           
                <?php $canEdit = $user->authorise('core.edit', 'com_dzguide'); ?>
                <?php if (!$canEdit && $user->authorise('core.edit.own', 'com_dzguide')): ?>
                    <?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
                <?php endif; ?>
                <tr>
                        <td width="90">
                            <a href="<?php echo JRoute::_('index.php?option=com_dzguide&view=guide&id='.(int) $item->id); ?>" title="<?php echo $this->escape($item->title); ?> by <?php echo JFactory::getUser($item->created_by)->name; ?>"><img src="<?php echo $item->hero_image['dota2']['hphover'];?>" class="img-thumb"/></a>
                        </td>
                        <td>
                            <h3 class="t1"><a href="<?php echo JRoute::_('index.php?option=com_dzguide&view=guide&id='.(int) $item->id); ?>" title="<?php echo $this->escape($item->title); ?> by <?php echo JFactory::getUser($item->created_by)->name; ?>"><?php echo $this->escape($item->title); ?></a></h3>
                            
                            <div class="small date"><i class="fa fa-pencil"></i> <?php echo JFactory::getUser($item->created_by)->name; ?> - <i class="fa fa-clock-o"></i> <?php echo JHtml::_('date', $item->modified, JText::_('DZ_DATE_FORMAT')); ?> - <i class="fa fa-shield"></i> <strong><?php echo $item->patch_version;?></strong></div>
                        </td>
                        <td class="text-center" width="12%">
                        <?php if ($userId) : ?>
                            <a href="#" class="btn btn-success btn-xs btn-vote<?= ($item->uservote === "1") ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="guide" data-direction="up">
                                <i class="fa fa-star"><span data-vote-count-of="<?= $item->id; ?>" data-vote-direction="up"><?= $item->upvotes; ?></span></i>
                            </a>
                        <?php else : ?>
                            <a href="#" class="btn btn-success btn-xs btn-vote" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                                <i class="fa fa-star"><?= $item->upvotes; ?></i>
                            </a>
                        <?php endif; ?>
                        <?php if ($userId) : ?>
                            <a href="#" class="btn btn-xs btn-vote<?= ($item->uservote === "0") ? ' active' : ''; ?>" data-vote="<?= $item->id; ?>" data-type="guide" data-direction="down">
                                <i class="fa fa-thumbs-down"><span data-vote-count-of="<?= $item->id; ?>" data-vote-direction="down"><?= $item->downvotes; ?></span></i>
                            </a>
                        <?php else : ?>
                            <a href="#" class="btn btn-xs btn-vote" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                                <i class="fa fa-thumbs-down"><?= $item->downvotes; ?></i>
                            </a>
                        <?php endif; ?>
                        </td>
                        <td class="text-right hidden-xs">
                        <div class="fb-like" data-href="<?php echo JRoute::_('index.php?option=com_dzguide&view=guide&id='.(int) $item->id); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
                        </td>
                </tr>
                
            <?php endforeach; ?>
            </table>
    	</div>
        <div class="component-footer">
    		<?php echo $this->pagination->getPagesLinks(); ?>
    	</div>
    </div>
</div>

</form>
