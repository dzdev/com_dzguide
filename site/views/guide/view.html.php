<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class DzguideViewGuide extends JViewLegacy {

    protected $state;
    protected $item;
    protected $form;
    protected $params;

    /**
     * Display the view
     */
    public function display($tpl = null) {

        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $dispatcher = JEventDispatcher::getInstance();

        $this->state = $this->get('State');
        $this->item = $this->get('Data');
        $this->params = $app->getParams('com_dzguide');

        if (!empty($this->item)) {
            
		$this->form		= $this->get('Form');
        }


        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }
        
        if (!$this->item) {
            JError::raiseError(404, "Not found");
        }

        

        if ($this->_layout == 'edit') {
            $authorised = $user->authorise('core.create', 'com_dzguide');
            if ($authorised !== true) {
                throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }
        
        if ($this->item->state != 1 && (
            $this->item->created_by != $user->get('id') ||
            $user->authorise('core.edit', 'com_dzguide.guide.' . $this->item->id)
        )) {
            JError::raiseError(403, JText::_('JLIB_APPLICATION_ERROR_ACCESS_FORBIDDEN'));
        }
        
        JPluginHelper::importPlugin('dz');
		$dispatcher->trigger('onTextPrepare', array ('com_dzguide.guide.description', &$this->item->description));
        $dispatcher->trigger('onTextPrepare', array ('com_dzguide.guide.content', &$this->item->content));
        
        if (is_array($this->item->builds)) {
            foreach ($this->item->builds as &$build) {
                $dispatcher->trigger('onTextPrepare', array ('com_dzguide.build.description', &$build->description));
            }
        }
        
        $model = $this->getModel();
		$model->hit();

        $this->_prepareDocument();

        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_DZGUIDE_DEFAULT_PAGE_TITLE'));
        }
        $title = $this->params->get('page_title', '');
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description', $this->item->metadesc));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords', $this->item->metakey));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }
    }

}
