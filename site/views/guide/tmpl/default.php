<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_dzguide.' . $this->item->id);
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_dzguide' . $this->item->id)) {
    $canEdit = JFactory::getUser()->id == $this->item->created_by;
}
$userId = JFactory::getUser()->get('id');
JHtml::_('jquery.framework');
JHtml::script('com_dzguide/vote.js', false, true);
?>
<div class="component-inner">
<?php if ($this->item) : ?>

    <div class="item_fields">
        <table class="table">
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_ID'); ?></th>
                <td><?php echo $this->item->id; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_STATE'); ?></th>
                <td>
                    <i class="icon-<?php echo ($this->item->state == 1) ? 'publish' : 'unpublish'; ?>"></i></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_CREATED'); ?></th>
                <td><?php echo $this->item->created; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_CREATED_BY'); ?></th>
                <td><?php echo $this->item->created_by_name; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_MODIFIED'); ?></th>
                <td><?php echo $this->item->modified; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_MODIFIED_BY'); ?></th>
                <td><?php echo $this->item->modified_by_name; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_TITLE'); ?></th>
                <td><?php echo $this->item->title; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_ALIAS'); ?></th>
                <td><?php echo $this->item->alias; ?></td>
            </tr>
            <tr>
                <th>Votes</th>
                <td>
                    <?php if ($userId) : ?>
                        <a href="#" class="btn btn-success btn-xs<?= ($this->item->userVote === "1") ? ' active' : ''; ?>" data-vote="<?= $this->item->id; ?>" data-type="guide" data-direction="up">
                            <i class="fa fa-star"><span data-vote-count-of="<?= $this->item->id; ?>" data-vote-direction="up"><?= $this->item->upvotes; ?></span></i>
                        </a>
                    <?php else : ?>
                        <span class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                            <i class="fa fa-star"><?= $this->item->upvotes; ?></i>
                        </span>
                    <?php endif; ?>
                    <?php if ($userId) : ?>
                        <a href="#" class="btn btn-xs<?= ($this->item->userVote === "0") ? ' active' : ''; ?>" data-vote="<?= $this->item->id; ?>" data-type="guide" data-direction="down">
                            <i class="fa fa-thumbs-down"><span data-vote-count-of="<?= $this->item->id; ?>" data-vote-direction="down"><?= $this->item->downvotes; ?></span></i>
                        </a>
                    <?php else : ?>
                        <span class="btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="<?= JTEXT::_('COM_DZGUIDE_WARNING_LOG_IN_TO_VOTE'); ?>">
                            <i class="fa fa-thumbs-down"><?= $this->item->downvotes; ?></i>
                        </span>
                    <?php endif; ?>
                </td>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_DESCRIPTION'); ?></th>
                <td><?php echo $this->item->description; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_CONTENT'); ?></th>
                <td><?php echo $this->item->content; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_COVER'); ?></th>
                <td><?php echo $this->item->cover; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_TAGS'); ?></th>
                <td>
                    <?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
                    <?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
                </td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_HIT'); ?></th>
                <td><?php echo $this->item->hits; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_PARAMS'); ?></th>
                <td><?php echo $this->item->params; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_METAKEY'); ?></th>
                <td><?php echo $this->item->metakey; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_METADESC'); ?></th>
                <td><?php echo $this->item->metadesc; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_METADATA'); ?></th>
                <td><?php echo $this->item->metadata; ?></td>
            </tr>
            <tr>
                <th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_GUIDE_VERSION'); ?></th>
                <td><?php echo $this->item->patch_version; ?></td>
            </tr>
        </table>
        <div class="tabs">
            <ul class="nav nav-tabs">
                <?php foreach ($this->item->builds as $index => $build) : ?>
                <li<?= ($index == 0) ? ' class="active"' : '' ?>><a href="#build_<?= $index; ?>">Build <?= $index + 1 ?></a></li>
                <?php endforeach; ?>
            </ul>
            <div class="tab-content">
                <?php foreach ($this->item->builds as $index => $build) : ?>
                <div id="build_<?= $index ?>" class="tab-pane<?= ($index == 0) ? ' active' : '' ?>">
                    <h3><?= $build->title; ?></h3>
                    <table class="table">
                        <tr>
                            <th>Hero</th>
                            <td><?= $build->hero_dname; ?></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><?= $build->description; ?></td>
                        </tr>
                        <tr>
                            <th>Skills</th>
                            <td>
                                <?php if ($build->skills) : ?>
                                <table class="table table-bordered skill-table">
                                    <?php foreach(array('q', 'w', 'e', 'r', 'stats') as $key) : ?>
                                    <tr>
                                        <th><?= strtoupper($key) ?></th>
                                        <?php for ($i = 1; $i <= 25; $i++) : ?>
                                        <td <?= in_array($i, $build->skills->{$key}) ? 'style="background-color: #444" class="active"' : null; ?>>
                                            <?= $i; ?>
                                        </td>
                                        <?php endfor; ?>
                                    </tr>
                                    <?php endforeach; ?>
                                </table>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Items</th>
                            <td>
                                <?php foreach($build->items as $order) : ?>
                                    <div class="order-container">
                                        <h4><?= $order->name; ?></h4>
                                        <ul>
                                        <?php foreach ($order->items as $order_item) : ?>
                                            <li><img src="<?= $order_item->image->dota2->lg; ?>" width="32px" height="32px" title="<?= $order_item->dname; ?>"/></li>
                                        <?php endforeach; ?>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <script type="text/javascript">
        jQuery(".nav-tabs a").on('click', function (e) {
            e.preventDefault();
            jQuery(this).tab('show');
        });
        </script>
    </div>
    <?php if($canEdit && $this->item->checked_out == 0): ?>
        <a class="btn" href="<?php echo JRoute::_('index.php?option=com_dzguide&task=guide.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_DZGUIDE_EDIT_ITEM"); ?></a>
    <?php endif; ?>
    <?php if(JFactory::getUser()->authorise('core.delete','com_dzguide.guide.'.$this->item->id)):?>
        <a class="btn" href="<?php echo JRoute::_('index.php?option=com_dzguide&task=guide.remove&id=' . $this->item->id, false, 2); ?>"><?php echo JText::_("COM_DZGUIDE_DELETE_ITEM"); ?></a>
    <?php endif; ?>
    <?php
    else:
        echo JText::_('COM_DZGUIDE_ITEM_NOT_LOADED');
    endif;
    ?>
</div>
<style type="text/css">
.skill-table > tbody > tr > td {
    padding: 8px 4px;
    text-align: center;
    min-width: 26px;
}
.order-container {
    background-color: #333;
    padding: 5px 10px;
    display: inline-block;
}
.order-container h4 {
    border-bottom: 1px solid #aaa;
}
.order-container ul {
    list-style: none;
    margin: 0;
    padding: 0;
}
.order-container ul li {
    float: left;
    list-style: none;
    padding: 1px 5px;
}
.btn-vote.disabled:hover {
    color: inherit;
}
.btn-vote.active {
    color: red;
}
</style>
