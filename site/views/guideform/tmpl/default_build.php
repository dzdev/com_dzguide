<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

$form = $this->item->build_form;
?>

<div class="form-group">
    <div class="control-label"><?php echo $form->getLabel('title'); ?></div>
    <div class="controls"><?php echo $form->getInput('title'); ?></div>
</div>
<div class="form-group">
    <div class="control-label"><?php echo $form->getLabel('skills'); ?></div>
    <div class="controls"><?php echo $form->getInput('skills'); ?></div>
</div>
<div class="form-group">
    <div class="control-label"><?php echo $form->getLabel('items'); ?></div>
    <div class="controls"><?php echo $form->getInput('items'); ?></div>
</div>
<div class="form-group">
    <div class="control-label"><?php echo $form->getLabel('id'); ?></div>
    <div class="controls"><?php echo $form->getInput('id'); ?></div>
</div>
<div class="form-group">
    <div class="controls">
        <a href="#" onclick="removeBuild(this); return false;" class="btn btn-danger">
            <i class="icon-minus"></i><?= JText::_('COM_DZGUIDE_FORM_BTN_REMOVE_BUILD'); ?>
        </a>
    </div>
</div>
