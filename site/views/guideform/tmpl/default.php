<?php
/**
* @version     1.0.0
* @package     com_dzguide
* @copyright   Copyright (C) 2013. All rights reserved.
* @license     GNU General Public License version 2 or later; see LICENSE.txt
* @author      DZ Team <support@dezign.vn> - dezign.vn
*/
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dzguide', JPATH_ADMINISTRATOR);

if($this->item->id) {
    $canState = JFactory::getUser()->authorise('core.edit.state','com_dzguide.guide');
} else {
    $canState = JFactory::getUser()->authorise('core.edit.state','com_dzguide.guide.'.$this->item->id);
}
$add_build_title = JText::_('COM_DZGUIDE_TAB_TITLE_ADD_BUILD');
$new_build_title = JText::_('COM_DZGUIDE_TAB_TITLE_NEW_BUILD');
JFactory::getDocument()->addScriptDeclaration(<<<ADDTAB
jQuery(window).load(function() {
    var tab = jQuery('<li class=""><a id="add_build" href="#"><i class="fa fa-plus"></i> {$add_build_title}</a></li>');
    var nextBuildIndex = function() {
        var i = 0;
        while (jQuery("#build_" + i).length > 0) {
            i++;
        }
        return i;
    }
    removeBuild = function(button) {
        if (confirm("Are you sure?")) {
            var \$tab = jQuery(button).closest('.tab-pane');
            jQuery('li a[href="#' + \$tab.attr('id') + '"]').parent().remove();
            \$tab.remove();
        }
        return false;
    }
    jQuery('#build-nav').append(tab);
    jQuery('#add_build').on('click', function() {
        jQuery('#add_build .fa-plus').addClass('fa-spin');
        jQuery.get('/index.php?option=com_dzguide&task=guideform.add', function(html) {
            jQuery('#add_build .fa-plus').removeClass('fa-spin');
            var \$document = jQuery(html),
                index = nextBuildIndex(),
                \$tab_nav = jQuery('<li class="active"><a href="#build_' + index + '">{$new_build_title}</a></li>'),
                \$tab_content = jQuery(\$document.find('#build_0')[0].outerHTML.replace(/_0/g, '_' + index).replace(/\[0\]/g, '[' + index + ']'));
            
            // Deactivate current tab
            jQuery('#builds li.active, #builds .tab-pane.active').removeClass('active');
            
            // Add new tab to builds
            \$tab_nav.insertBefore('#build-nav li:last-child');
            \$tab_nav.find('a').on('click', function(e) {
                e.preventDefault();
                jQuery(this).tab('show');
            });
            angular.DZApp.initItemsApp(\$tab_content.find("[data-items-app]"));
            angular.DZApp.initSkillsApp(\$tab_content.find("[data-skills-app]"));
            \$tab_content.appendTo('#builds .tab-content');
        })
    });
    jQuery('#build-nav a').click(function(e) {
        e.preventDefault();
        jQuery(this).tab('show');
    });
});
ADDTAB
);
?>

<div class="component-inner">
    <div class="guide-edit front-end-edit">
        <?php if (!empty($this->item->id)): ?>
            <h1>Edit <?php echo $this->item->id; ?></h1>
        <?php else: ?>
            <h1>Submit New Guide</h1>
        <?php endif; ?>

        <form id="form-guide" action="<?php echo JRoute::_('index.php?option=com_dzguide&task=guide.save&id=' . $this->item->id); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= $this->form->getLabel('title'); ?>
                        <?= $this->form->getInput('title'); ?>
                    </div>
                    <div class="form-group">
                        <?= $this->form->getLabel('hero_id'); ?>
                        <?= $this->form->getInput('hero_id'); ?>
                    </div>
                    <div class="form-group">
                        <?= $this->form->getLabel('patch_version'); ?>
                        <?= $this->form->getInput('patch_version'); ?>
                    </div>
                    <div class="form-group">
                        <?= $this->form->getLabel('state'); ?>
                        <?= $this->form->getInput('state'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= $this->form->getLabel('description'); ?>
                        <?= $this->form->getInput('description'); ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Builds
                </div>
                <div id="builds" class="panel-body">
                    <ul class="nav nav-tabs" id="build-nav">
                    <?php if (isset($this->item->builds_model) && count($this->item->builds_model)) : ?>
                        <?php foreach($this->item->builds_model as $index => $build) : ?>
                        <li class="<?= ($index == 0) ? 'active' : ''; ?>">
                            <a href="#build_<?= $index; ?>" data-toggle="tab"><?= JText::sprintf('COM_DZGUIDE_TAB_TITLE_BUILD', $index + 1); ?></a>
                        </li>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <li class="active">
                            <a href="#build_0"><?= JText::_('COM_DZGUIDE_TAB_TITLE_NEW_BUILD'); ?></a>
                        </li>
                    <?php endif; ?>
                    </ul>
                    
                    <div class="tab-content" id="build-content">
                    <?php if (isset($this->item->builds_model) && count($this->item->builds_model)) : ?>
                        <?php foreach($this->item->builds_model as $index => $build) : ?>
                        <div class="tab-pane<?= ($index == 0) ? ' active' : ''; ?>" id="build_<?= $index; ?>">
                            <?php $this->item->build_form = JForm::getInstance('com_dzguide.build.' . $index, 'buildform', array('control' => "jform[builds][$index]")); ?>
                            <?php $this->item->build_form->bind($build); ?>
                            <?= $this->loadTemplate('build'); ?>
                        </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class="tab-pane active" id="build_0">
                            <?php $this->item->build_form = JForm::getInstance('com_dzguide.build.0', 'buildform', array('control' => "jform[builds][0]")); ?>
                            <?= $this->loadTemplate('build'); ?>
                        </div>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Content</div>
                <div class="panel-body"><?php echo $this->form->getInput('content'); ?></div>
            </div>
            <div class="form-group">
                <button type="submit" class="validate btn btn-primary"><?php echo JText::_('JSUBMIT'); ?></button>
            </div>

            <?php echo $this->form->getInput('id'); ?>
            <input type="hidden" name="option" value="com_dzguide" />
            <input type="hidden" name="task" value="guideform.apply" />
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<style type="text/css">
.panel {
    color: #333;
}
td {
    padding: 8px 6px !important;
}
td.active {
    background-color: #eee !important;
}
</style>
