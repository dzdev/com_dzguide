<?php
/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_dzguide.' . $this->item->id);
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_dzguide' . $this->item->id)) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

    <div class="item_fields">
        <table class="table">
            <tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_ID'); ?></th>
			<td><?php echo $this->item->id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_STATE'); ?></th>
			<td>
			<i class="icon-<?php echo ($this->item->state == 1) ? 'publish' : 'unpublish'; ?>"></i></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_CREATED'); ?></th>
			<td><?php echo $this->item->created; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_CREATED_BY'); ?></th>
			<td><?php echo $this->item->created_by_name; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_MODIFIED'); ?></th>
			<td><?php echo $this->item->modified; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_MODIFIED_BY'); ?></th>
			<td><?php echo $this->item->modified_by_name; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_TITLE'); ?></th>
			<td><?php echo $this->item->title; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_ALIAS'); ?></th>
			<td><?php echo $this->item->alias; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_HERO_ID'); ?></th>
			<td><?php echo $this->item->hero_id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_SKILLS'); ?></th>
			<td><?php echo $this->item->skills; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_ITEMS'); ?></th>
			<td><?php echo $this->item->items; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_PARAMS'); ?></th>
			<td><?php echo $this->item->params; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_DZGUIDE_FORM_LBL_BUILD_GUIDE_ID'); ?></th>
			<td><?php echo $this->item->guide_id; ?></td>
</tr>

        </table>
    </div>
    <?php if($canEdit && $this->item->checked_out == 0): ?>
		<a class="btn" href="<?php echo JRoute::_('index.php?option=com_dzguide&task=build.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_DZGUIDE_EDIT_ITEM"); ?></a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_dzguide.build.'.$this->item->id)):?>
									<a class="btn" href="<?php echo JRoute::_('index.php?option=com_dzguide&task=build.remove&id=' . $this->item->id, false, 2); ?>"><?php echo JText::_("COM_DZGUIDE_DELETE_ITEM"); ?></a>
								<?php endif; ?>
    <?php
else:
    echo JText::_('COM_DZGUIDE_ITEM_NOT_LOADED');
endif;
?>
