<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// No direct access
defined('_JEXEC') or die;

/**
 * @param	array	A named array
 * @return	array
 */
function DzguideBuildRoute(&$query) {
    $segments = array();
    
    // get a menu item based on Itemid or currently active
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    
    // we need a menu item.  Either the one specified in the query, or the current active one if none specified
    if (empty($query['Itemid'])) {
        $menuItem = $menu->getActive();
    } else {
        $menuItem = $menu->getItem($query['Itemid']);
    }
    
    $mView = (empty($menuItem->query['view'])) ? null : $menuItem->query['view'];
    $mId = (empty($menuItem->query['id'])) ? null : $menuItem->query['id'];

    if (isset($query['view'])) {
        if (!($mView == 'guides' && $query['view'] == 'guide' || $query['view'] == $mView))
            $segments[] = $query['view'];
        unset($query['view']);
    }
    if ($mView == 'guides' && isset($query['hero_name'])) {
        $segments[] = $query['hero_name'];
        unset($query['hero_name']);
    }
    if (isset($query['id'])) {
        if ($mId != $query['id']) {
            $segments[] = $query['id'];
        }
        unset($query['id']);
    }
    

    return $segments;
}

/**
 * @param	array	A named array
 * @param	array
 *
 * Formats:
 *
 * index.php?/dzguide/task/id/Itemid
 *
 * index.php?/dzguide/id/Itemid
 */
function DzguideParseRoute($segments) {
    $vars = array();
    
    //Get the active menu item.
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $item = $menu->getActive();

    // Count segments
    $count = count($segments);
    if ($count == 1 && $item && (int) $segments[0]) {
        // This mean we only have the id, view is implied from the menu
        if ($item) {
            if ($item->query['view'] == 'guides') {
                $vars['view'] = 'guide';
            } else if ($item->query['view'] == 'builds') {
                $vars['view'] = 'build';
            }
        }
    } else {
        if ($count == 1) {
            // Check if this is a hero alias
            $db = JFactory::getDbo();
            $db->setQuery("SELECT id FROM #__dota2_heroes WHERE name = " . $db->quote($segments[0]));
            $id = $db->loadResult();
            // var_dump($id); die;
            if ($id) {
                $vars['hero_id'] = $id;
                $vars['view'] = $item ? $item->query['view'] : 'guides';
                array_shift($segments);
            } else {
                $vars['view'] = array_shift($segments);
            }
        } else {
            // View is always first in other cases
            $vars['view'] = array_shift($segments);
        }
    }
    
    // Id is always last segment
    if (count($segments)) {
        $vars['id'] = array_pop($segments);
    }
    return $vars;
}
