<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
require_once JPATH_SITE . '/components/com_dzguide/helpers/route.php';

/**
 * Methods supporting a list of Dzguide records.
 */
class DzguideModelGuides extends JModelList
{

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     *
     * @see        JController
     * @since      1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created', 'a.created',
                'created_by', 'a.created_by',
                'modified', 'a.modified',
                'modified_by', 'a.modified_by',
                'title', 'a.title',
                'alias', 'a.alias',
                'description', 'a.description',
                'content', 'a.content',
                'cover', 'a.cover',
                'tags', 'a.tags',
                'hits', 'a.hits',
                'params', 'a.params',
                'metakey', 'a.metakey',
                'metadesc', 'a.metadesc',
                'metadata', 'a.metadata',
                'patch_version', 'a.patch_version',
                'dh.id'
            );
        }
        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {


        // Initialise variables.
        $app = JFactory::getApplication();

        // List state information
        $limit = $app->input->get('limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = $app->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);
        
        $list = array();
        $list['ordering'] = $app->input->get('filter_order', 'a.modified');
        if (!in_array($list['ordering'], $this->filter_fields)) {
            $list['ordering'] = 'a.modified';
        }
        $list['direction'] = $app->input->get('filter_order_Dir', 'desc');
        if (!in_array(strtoupper($list['direction']), array('ASC', 'DESC', ''))) {
            $list['direction'] = 'desc';
        }
        
        $this->setState('list.ordering', $list['ordering']);
        $this->setState('list.direction', $list['direction']);

        if ($list = $app->input->get('list', array(), 'array'))
        {
            foreach ($list as $name => $value)
            {
                // Extra validations
                switch ($name)
                {
                    case 'fullordering':
                        $orderingParts = explode(' ', $value);

                        if (count($orderingParts) >= 2)
                        {
                            // Latest part will be considered the direction
                            $fullDirection = end($orderingParts);

                            if (in_array(strtoupper($fullDirection), array('ASC', 'DESC', '')))
                            {
                                $this->setState('list.direction', $fullDirection);
                            }

                            unset($orderingParts[count($orderingParts) - 1]);

                            // The rest will be the ordering
                            $fullOrdering = implode(' ', $orderingParts);

                            if (in_array($fullOrdering, $this->filter_fields))
                            {
                                $this->setState('list.ordering', $fullOrdering);
                            }
                        }
                        else
                        {
                            $this->setState('list.ordering', $list['ordering']);
                            $this->setState('list.direction', $list['direction']);
                        }
                        break;

                    case 'ordering':
                        if (!in_array($value, $this->filter_fields))
                        {
                            $value = $list['ordering'];
                        }
                        break;

                    case 'direction':
                        if (!in_array(strtoupper($value), array('ASC', 'DESC', '')))
                        {
                            $value = $list['direction'];
                        }
                        break;

                    case 'limit':
                        $limit = $value;
                        break;

                    // Just to keep the default case
                    default:
                        $value = $value;
                        break;
                }

                $this->setState('list.' . $name, $value);
            }
        }

        // Receive & set filters
        if ($filters = $app->input->get('filter', array(), 'array'))
        {
            foreach ($filters as $name => $value)
            {
                $this->setState('filter.' . $name, $value);
            }
        }
        
        // Search by heroes
        $filter_hero = $app->input->getInt('hero_id');
        if ($filter_hero) {
            $this->setState('filter.hero_id', $filter_hero);
        }
        
        // Search by text
        $filter_search = $app->input->getString('q');
        if ($filter_search) {
            $this->setState('filter.search', $filter_search);
        }
        
        // Filter by Dota 2 patch version
        $filter_version = $app->input->getCmd('v');
        if ($filter_version) {
            $this->setState('filter.patch_version', $filter_version);
        }
        
        // Ordering
        $orderingParts = explode(' ', $app->input->getString('order'));

        if (count($orderingParts) >= 2)
        {
            // Latest part will be considered the direction
            $fullDirection = end($orderingParts);

            if (in_array(strtoupper($fullDirection), array('ASC', 'DESC', '')))
            {
                $this->setState('list.direction', $fullDirection);
            }

            unset($orderingParts[count($orderingParts) - 1]);

            // The rest will be the ordering
            $fullOrdering = implode(' ', $orderingParts);

            if (in_array($fullOrdering, $this->filter_fields))
            {
                $this->setState('list.ordering', $fullOrdering);
            }
        }
        
        $display_mode = $app->input->get('display_mode', 'public');
        if (!in_array(strtolower($display_mode), array('public', 'private'))) {
            $display_mode = 'public';
        }
        $this->setState('list.mode', $display_mode);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return    JDatabaseQuery
     * @since    1.6
     */
    protected function getListQuery()
    {
        // Create a new query object.
        $db    = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query
            ->select(
                $this->getState(
                    'list.select', 'DISTINCT a.*'
                )
            );

        $query->from('`#__dzguide_guides` AS a');

        
        // Join over the users for the checked out user.
        $query->select('uc.name AS editor');
        $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');
    
        // Join over the created by field 'created_by'
        $query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
        
        // Join over heroes
        $query->select('dh.dname as hero_dname, dh.name as hero_name');
        $query->join('LEFT', '#__dota2_heroes as dh ON a.hero_id = dh.id');

        // Get upvote count
        $upvote_subquery = $db->getQuery(true);
        $upvote_subquery->select('COUNT(*)')
            ->from('#__dzguide_ratings AS r1')
            ->where('a.id = r1.guide_id')
            ->where('r1.useful = 1')
            ->where('r1.state = 1');
        $query->select('(' . (string) $upvote_subquery . ') AS upvotes');
        
        // Get downvote
        $downvote_subquery = $db->getQuery(true);
        $downvote_subquery->select('COUNT(*)')
            ->from('#__dzguide_ratings AS r2')
            ->where('a.id = r2.guide_id')
            ->where('r2.useful = 0')
            ->where('r2.state = 1');
        $query->select('(' . (string) $downvote_subquery . ') AS downvotes');
        
        // Get current user vote
        $uservote_subquery = $db->getQuery(true);
        $uservote_subquery->select('r3.useful')
            ->from('#__dzguide_ratings AS r3')
            ->where('a.id = r3.guide_id')
            ->where('r3.state = 1')
            ->where('r3.created_by = ' . JFactory::getUser()->get('id'));
        $query->select('('. (string) $uservote_subquery . ') AS uservote');
        
        $display_mode = $this->getState('list.mode');
        if (!$display_mode || $display_mode == 'public') {
            $query->where('a.state = 1');
        } else if ($display_mode == 'private') {
            $query->where('a.created_by = ' . JFactory::getUser()->get('id'));
        }
        
        $tags = $this->getState('filter.tags');
        if ($tags) {
            if (!is_array($tags)) {
                $tags = explode(',', $tags);
            }
            JArrayHelper::toInteger($tags);
            $query->join('INNER', '#__contentitem_tag_map as t ON t.type_alias = \'com_dzguide.guide\' AND t.content_item_id = a.id AND t.tag_id IN (' . implode(',', $tags) .')');
        }

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search))
        {
            if (stripos($search, 'id:') === 0)
            {
                $query->where('a.id = ' . (int) substr($search, 3));
            }
            else
            {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.title LIKE '.$search.' )');
            }
        }

        // Filtering author
        $filter_created_by = $this->state->get("filter.created_by");
        if ($filter_created_by) {
            $query->where("a.created_by = " . (int) $filter_created_by);
        }

        //Filtering version
        $filter_version = $this->state->get("filter.patch_version");
        if ($filter_version) {
            $query->where("a.patch_version = '".$db->escape($filter_version)."'");
        }
        
        //Filtering hero
        $filter_hero = $this->state->get("filter.hero_id");
        if ($filter_hero) {
            $query->where("dh.id = '".$db->escape($filter_hero)."'");
        }
        
        // Exclude
        $filter_exclude = $this->state->get("filter.exclude_id");
        if ($filter_exclude) {
            if (!is_array($filter_exclude)) {
                $filter_exclude = explode(',', (string) $filter_exclude);
            }
            JArrayHelper::toInteger($filter_exclude);
            $query->where('a.id NOT IN (' . implode(',', $filter_exclude) . ')');
        }


        // Add the list ordering clause.
        $orderCol  = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn)
        {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems()
    {
        $items = parent::getItems();
        foreach($items as $item) {
            if ( isset($item->tags) ) {
                // Catch the item tags (string with ',' coma glue)
                $tags = explode(",",$item->tags);

                $db = JFactory::getDbo();
                $namedTags = array(); // Cleaning and initalization of named tags array

                // Get the tag names of each tag id
                foreach ($tags as $tag) {

                    $query = $db->getQuery(true);
                    $query->select("title");
                    $query->from('`#__tags`');
                    $query->where( "id=" . intval($tag) );

                    $db->setQuery($query);
                    $row = $db->loadObjectList();

                    // Read the row and get the tag name (title)
                    if (!is_null($row)) {
                        foreach ($row as $value) {
                            if ( $value && isset($value->title) ) {
                                $namedTags[] = trim($value->title);
                            }
                        }
                    }

                }

                // Finally replace the data object with proper information
                $item->tags = !empty($namedTags) ? implode(', ',$namedTags) : $item->tags;
            }
            $item->hero_image = array(
                'dota2' => array(
                    'full'  => JUri::root().'media/com_dota2/images/dota2/heroes/'.$item->hero_name.'_full.png',
                    'hphover' => JUri::root().'media/com_dota2/images/dota2/heroes/'.$item->hero_name.'_hphover.png',
                    'sb'    => JUri::root().'media/com_dota2/images/dota2/heroes/'.$item->hero_name.'_sb.png',
                    'vert'  => JUri::root().'media/com_dota2/images/dota2/heroes/'.$item->hero_name.'_vert.jpg'
                ),
                'dota1' => array(
                    'full'    => JUri::root().'media/com_dota2/images/dota1/heroes/'.$item->hero_name.'_full.png',
                    'hphover' => JUri::root().'media/com_dota2/images/dota1/heroes/'.$item->hero_name.'_hphover.png'
                )
            );
            $item->link = JRoute::_(DZGuideHelperRoute::getGuideRoute($item->id . ':' . $item->alias));
        }

        return $items;
    }
    
    public function getHeroes()
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('DISTINCT h.id, name, dname');
        $query->from('#__dota2_heroes as h');
        $query->join('INNER', '#__dzguide_guides as g ON g.hero_id = h.id');
        $query->order('dname asc');
        $db->setQuery($query);
        $heroes = $db->loadObjectList();
        
        foreach ($heroes as &$hero) {
            $hero->image = JUri::root().'media/com_dota2/images/dota2/heroes/'.$hero->name.'_full.png';
        }
        
        return $heroes;
    }

    /**
     * Overrides the default function to check Date fields format, identified by
     * "_dateformat" suffix, and erases the field if it's not correct.
     */
    protected function loadFormData()
    {
        $app              = JFactory::getApplication();
        $filters          = $app->getUserState($this->context . '.filter', array());
        $error_dateformat = false;
        foreach ($filters as $key => $value)
        {
            if (strpos($key, '_dateformat') && !empty($value) && !$this->isValidDate($value))
            {
                $filters[$key]    = '';
                $error_dateformat = true;
            }
        }
        if ($error_dateformat)
        {
            $app->enqueueMessage(JText::_("COM_DZGUIDE_SEARCH_FILTER_DATE_FORMAT"), "warning");
            $app->setUserState($this->context . '.filter', $filters);
        }

        return parent::loadFormData();
    }

    /**
     * Checks if a given date is valid and in an specified format (YYYY-MM-DD)
     *
     * @param string Contains the date to be checked
     *
     */
    private function isValidDate($date)
    {
        return preg_match("/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/", $date) && date_create($date);
    }

}
