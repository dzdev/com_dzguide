<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Dzguide records.
 */
class DzguideModelAuthors extends JModelList
{
    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     *
     * @see        JController
     * @since      1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'u.id',
                'u.name',
                'u.registerDate',
                'guide_count',
                'upvote_count',
                'downvote_count',
            );
        }
        parent::__construct($config);
    }
    
    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {


        // Initialise variables.
        $app = JFactory::getApplication();

        // List state information
        $limit = $app->input->getInt('limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = $app->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);
        
        $list['ordering'] = $app->input->getCmd('filter_order', 'u.registerDate');
        if (!in_array($list['ordering'], $this->filter_fields)) {
            $list['ordering'] = 'a.modified';
        }
        
        $list['direction'] = $app->input->get('filter_order_Dir', 'desc');
        if (!in_array(strtoupper($list['direction']), array('ASC', 'DESC', ''))) {
            $listOrder = 'desc';
        }
        
        $this->setState('list.ordering', $list['ordering']);
        $this->setState('list.direction', $list['direction']);
    }
    
    /**
     * Build an SQL query to load the list data.
     *
     * @return    JDatabaseQuery
     * @since    1.6
     */
    protected function getListQuery()
    {
        // Create a new query object.
        $db    = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query
            ->select(
                $this->getState(
                    'list.select', 'u.*'
                )
            );

        $query->from('`#__users` AS u');
        
        // Join over guides table
        $guides_query = $db->getQuery(true);
        $guides_query->select('created_by, COUNT(g.id) as count');
        $guides_query->from('#__dzguide_guides as g');
        $guides_query->where('g.state = 1');
        $guides_query->group('created_by');
        $query->select('COALESCE(g.count, 0) as guide_count');
        $query->join('LEFT', "($guides_query) as g ON g.created_by = u.id");
        
        // Join over votes table
        $votes_query = $db->getQuery(true);
        $votes_query->select('u.id as uid, SUM(useful) as upvote_count, COUNT(case useful WHEN 0 then 0 else null end) as downvote_count');
        $votes_query->from('#__dzguide_ratings as r');
        $votes_query->where('r.state = 1');
        $votes_query->join('INNER', '#__dzguide_guides as g ON g.id = r.guide_id');
        $votes_query->join('INNER', '#__users as u ON u.id = g.created_by');
        $votes_query->group('u.id');
        $query->select('COALESCE(r.upvote_count, 0) as upvote_count, COALESCE(r.downvote_count, 0) as downvote_count');
        $query->join('LEFT', "($votes_query) as r ON u.id = r.uid");
        
        // Join over steamid table
        $query->select('s.steamid, s.personaname, s.realname, s.profileurl, s.avatar');
        $query->join('LEFT', '#__steamid as s ON s.user_id = u.id');
        
        // Exclude ids
        $exclude_id = $this->state->get('filter.exclude_id');
        if ($exclude_id) {
            if (!is_array($exclude_id)) {
                $exclude_id = explode(',', $exclude_id);
            }
            JArrayHelper::toInteger($exclude_id);
            $query->where('u.id NOT IN (' . implode(',', $exclude_id) . ')');
        }
        
        // Add the list ordering clause.
        $orderCol  = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn)
        {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }
}
