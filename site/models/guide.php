<?php

/**
 * @version     1.0.0
 * @package     com_dzguide
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

/**
 * Dzguide model.
 */
class DzguideModelGuide extends JModelItem {

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState() {
        $app = JFactory::getApplication('com_dzguide');

        // Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit') {
            $id = JFactory::getApplication()->getUserState('com_dzguide.edit.guide.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_dzguide.edit.guide.id', $id);
        }
        $this->setState('guide.id', $id);

        // Load the parameters.
        $params = $app->getParams();
        $params_array = $params->toArray();
        if (isset($params_array['item_id'])) {
            $this->setState('guide.id', $params_array['item_id']);
        }
        $this->setState('params', $params);
    }

    /**
     * Method to get an ojbect.
     *
     * @param    integer    The id of the object to get.
     *
     * @return    mixed    Object on success, false on failure.
     */
    public function &getData($id = null) {
        if ($this->_item === null) {
            $this->_item = false;

            if (empty($id)) {
                $id = $this->getState('guide.id');
            }

            // Get a level row instance.
            $table = $this->getTable();

            // Attempt to load the row.
            if ($table->load($id)) {
                // Check published state.
                if ($published = $this->getState('filter.published')) {
                    if ($table->state != $published) {
                        return $this->_item;
                    }
                }
                
                // Convert the JTable to a clean JObject.
                $properties = $table->getProperties(1);
                $this->_item = JArrayHelper::toObject($properties, 'JObject');
            } else {
                if ($error = $table->getError()) {
                    $this->setError($error);
                }
                return false;
            }
        }

        
        if ( isset($this->_item->created_by) ) {
            $this->_item->created_by_name = JFactory::getUser($this->_item->created_by)->name;
        }
        if ( isset($this->_item->modified_by) ) {
            $this->_item->modified_by_name = JFactory::getUser($this->_item->modified_by)->name;
        }

        $this->_item->tags = new JHelperTags;
        $this->_item->tags->getItemTags('com_dzguide.guide', $this->_item->id);
        
        $builds_model = JModelLegacy::getInstance('Builds', 'DZGuideModel', array('ignore_request' => true));
        $builds_model->setState('filter.guide_id', (int) $this->_item->id);
        $this->_item->builds = $builds_model->getItems();
        
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        // Get upvote count
        $upvote_subquery = $db->getQuery(true);
        $upvote_subquery->select('COUNT(*)')
            ->from('#__dzguide_ratings AS r1')
            ->where('r1.guide_id = ' . (int) $this->_item->id)
            ->where('r1.useful = 1')
            ->where('r1.state = 1');
        $query->select('(' . (string) $upvote_subquery . ') AS upvotes');
        
        // Get downvote
        $downvote_subquery = $db->getQuery(true);
        $downvote_subquery->select('COUNT(*)')
            ->from('#__dzguide_ratings AS r2')
            ->where('r2.guide_id = ' . (int) $this->_item->id)
            ->where('r2.useful = 0')
            ->where('r2.state = 1');
        $query->select('(' . (string) $downvote_subquery . ') AS downvotes');
        
        // Get current user vote
        $uservote_subquery = $db->getQuery(true);
        $uservote_subquery->select('r3.useful')
            ->from('#__dzguide_ratings AS r3')
            ->where('r3.guide_id = ' . (int) $this->_item->id)
            ->where('r3.state = 1')
            ->where('r3.created_by = ' . JFactory::getUser()->get('id'));
        $query->select('('. (string) $uservote_subquery . ') AS uservote');
        
        $db->setQuery($query);
        $votes = $db->loadObject();
        $this->_item->downvotes = $votes->downvotes;
        $this->_item->upvotes = $votes->upvotes;
        $this->_item->userVote = $votes->uservote;

        // Hero
        JModelLegacy::addIncludePath(JPATH_BASE . '/components/com_dota2/models');
        $hero_model = JModelLegacy::getInstance('Hero', 'Dota2Model', array('ignore_request' => true));
        $this->_item->hero = $hero_model->getData($this->_item->hero_id);
        
        return $this->_item;
    }

    public function getTable($type = 'Guide', $prefix = 'DzguideTable', $config = array()) {
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to check in an item.
     *
     * @param    integer        The id of the row to check out.
     * @return    boolean        True on success, false on failure.
     * @since    1.6
     */
    public function checkin($id = null) {
        // Get the id.
        $id = (!empty($id)) ? $id : (int) $this->getState('guide.id');

        if ($id) {

            // Initialise the table
            $table = $this->getTable();

            // Attempt to check the row in.
            if (method_exists($table, 'checkin')) {
                if (!$table->checkin($id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method to check out an item for editing.
     *
     * @param    integer        The id of the row to check out.
     * @return    boolean        True on success, false on failure.
     * @since    1.6
     */
    public function checkout($id = null) {
        // Get the user id.
        $id = (!empty($id)) ? $id : (int) $this->getState('guide.id');

        if ($id) {

            // Initialise the table
            $table = $this->getTable();

            // Get the current user object.
            $user = JFactory::getUser();

            // Attempt to check the row out.
            if (method_exists($table, 'checkout')) {
                if (!$table->checkout($user->get('id'), $id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
        }

        return true;
    }

    public function getCategoryName($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
                ->select('title')
                ->from('#__categories')
                ->where('id = ' . $id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function publish($id, $state) {
        $table = $this->getTable();
        $table->load($id);
        $table->state = $state;
        return $table->store();
    }

    public function delete($id) {
        $table = $this->getTable();
        return $table->delete($id);
    }
    
    /**
     * Increment the hit counter for the article.
     *
     * @param   integer  $pk  Optional primary key of the article to increment.
     *
     * @return  boolean  True if successful; false otherwise and internal error set.
     */
    public function hit($pk = 0)
    {
        $input = JFactory::getApplication()->input;
        $hitcount = $input->getInt('hitcount', 1);

        if ($hitcount)
        {
            $pk = (!empty($pk)) ? $pk : (int) $this->getState('guide.id');

            $table = JTable::getInstance('Guide', 'DZGuideTable');
            $table->load($pk);
            $table->hit($pk);
        }

        return true;
    }

}
